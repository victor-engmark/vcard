default:
  image: nixos/nix:2.26.3@sha256:cf7393e408da5ad343dad43670be72d7ee062b2a6a687990e9613ef9dc8bf2f6

workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

stages:
  - test
  - build
  - build-test
  - maintenance

lint files:
  stage: test
  script: nix-shell --pure --run 'pre-commit run --all-files'

lint commit messages:
  stage: test
  variables:
    GIT_DEPTH: ""
  script:
    - command=(gitlint --commits="origin/${CI_DEFAULT_BRANCH}..origin/${CI_COMMIT_REF_NAME}" --debug
      --fail-without-commits)
    - nix-shell --pure --run "${command[*]}"

test Nix build:
  stage: test
  script: nix-shell --pure --run 'vcard --help'

test Python code:
  stage: test
  script: nix-shell --pure --run ./test.bash
  coverage: /TOTAL.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

test pre-commit hook:
  stage: test
  script: nix-shell --pure --run 'pre-commit try-repo --files=test/valid.vcf -- .'

test clean:
  stage: test
  script:
    nix-shell --pure --run './test.bash && ./distribute.bash && ./clean.bash && ./test-clean.bash'

build Python package:
  stage: build
  script: nix-shell --pure --run ./distribute.bash
  artifacts:
    paths:
      - dist/*
    expire_in: 1 day

build IntelliJ SDK plugin:
  stage: build
  script:
    - nix-shell --pure --run 'cd intellij-sdk-plugin && gradle --no-daemon jar'
    - nix-shell --pure --run 'cd intellij-sdk-plugin && gradle --no-daemon --offline build'
  artifacts:
    paths:
      - intellij-sdk-plugin/build/libs/*.jar
    expire_in: 1 day

.test:
  stage: build-test
  needs:
    - job: build Python package
      artifacts: true
  script:
    - pip install dist/*
    - vcard --help

test install on Python 3.8:
  extends: .test
  image: python:3.8-alpine@sha256:3d93b1f77efce339aa77db726656872517b0d67837989aa7c4b35bd5ae7e81ba

test install on Python 3.9:
  extends: .test
  image: python:3.9-alpine@sha256:e345f1410de8c8c40a0afac784deabce796a52f26965c41290a710d4fb47fabe

test install on Python 3.10:
  extends: .test
  image: python:3.10-alpine@sha256:4c4097e46608e9b9025a486e3b72d628ac8947a3caa20f142c2e8c0029a12b21

test install on Python 3.11:
  extends: .test
  image: python:3.11-alpine@sha256:d5e2fc72296647869f5eeb09e7741088a1841195059de842b05b94cb9d3771bb

test install on Python 3.12:
  extends: .test
  image: python:3.12-alpine@sha256:28b8a72c4e0704dd2048b79830e692e94ac2d43d30c914d54def6abf74448a4e

test install on Python 3.13:
  extends: .test
  image: python:3.13-rc-alpine@sha256:5b0eec332a70e36e9303d9df9afb33714bb1b35a26e8d69adfcb9f03c1ae6e2a

sast:
  stage: test
  variables:
    SECURE_LOG_LEVEL: "debug"

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
