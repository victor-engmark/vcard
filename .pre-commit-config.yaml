# Configuration file for pre-commit (https://pre-commit.com/).
# Please run `pre-commit run --all-files` when adding or changing entries.

repos:
  - repo: local
    hooks:
      - id: black
        name: black
        entry: black
        language: system
        stages: [pre-commit]
        types: [python]

      - id: check-gitlab-ci
        name: check-gitlab-ci
        entry: check-jsonschema
        args: [--builtin-schema, vendor.gitlab-ci, --data-transform, gitlab-ci]
        files: ^\.gitlab-ci.yml$
        language: system
        stages: [pre-commit]

      - id: check-manifest
        name: check-manifest
        entry: check-manifest
        language: system
        pass_filenames: false
        stages: [pre-commit]

      - id: gitlint
        name: gitlint
        entry: gitlint
        args: [--msg-filename]
        language: system
        stages: [commit-msg]

      - id: isort
        name: isort
        entry: isort
        language: system
        stages: [pre-commit]
        types: [python]

      - id: mypy
        name: mypy
        entry: mypy
        args: [--no-incremental]
        language: system
        stages: [pre-commit]
        types: [python]
        require_serial: true

      - id: nixfmt
        name: nixfmt
        entry: nixfmt
        language: system
        files: \.nix$
        stages: [pre-commit]

      - id: prettier
        name: Prettier
        entry: prettier
        args: [--ignore-unknown, --list-different, --write]
        types: [text]
        language: system
        stages: [pre-commit]

      - id: pylint
        name: pylint
        entry: pylint
        language: system
        stages: [pre-commit]
        types: [python]

      - id: shellcheck
        name: shellcheck
        entry: shellcheck
        args: [--enable=all, --external-sources, --severity=style, --source-path=SCRIPTDIR]
        language: system
        stages: [pre-commit]
        types: [shell]

      - id: shfmt
        name: shfmt
        entry: shfmt
        args: [-l, -w]
        language: system
        types: [shell]
        stages: [pre-commit]

      - id: taplo
        name: taplo
        entry: taplo
        args: [format]
        language: system
        types: [toml]
        stages: [pre-commit]

  - repo: https://gitlab.com/engmark/shellcheck-gitlab-ci-scripts-hook
    rev: 48adb4e0b14aad9d657771ff0720b592652d266b # frozen: v2
    hooks:
      - id: shellcheck-gitlab-ci-scripts
        files: ^\.gitlab-ci\.yml$

  - repo: https://gitlab.com/engmark/sort-hook
    rev: 4b2564c7602aa1510034659de6cbb341172f42df # frozen: v3
    hooks:
      - id: sort
        args: [--locale=en_NZ.UTF-8]
        types_or: [gitignore]
        stages: [pre-commit]

  - repo: https://github.com/macisamuele/language-formatters-pre-commit-hooks
    rev: a6273196190bb0f68caf1dc68073cf62c719f725 # frozen: v2.14.0
    hooks:
      - id: pretty-format-ini
        name: Pretty format INI
        description: This hook sets a standard for formatting INI files.
        entry: pretty-format-ini
        args: [--autofix]
        language: python
        types: [ini]
        stages: [pre-commit]

  - repo: meta
    hooks:
      - id: check-hooks-apply
      - id: check-useless-excludes
