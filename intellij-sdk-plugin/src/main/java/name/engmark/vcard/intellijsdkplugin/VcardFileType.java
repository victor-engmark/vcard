package name.engmark.vcard.intellijsdkplugin;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.NlsContexts;
import com.intellij.openapi.util.NlsSafe;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class VcardFileType extends LanguageFileType {
  public static final VcardFileType INSTANCE = new VcardFileType();

  private VcardFileType() {
    super(VcardLanguage.INSTANCE);
  }

    @Override
    public @NonNls @NotNull String getName() {
        return "vCard";
    }

    @Override
    public @NlsContexts.Label @NotNull String getDescription() {
        return "vCard MIME Directory Profile; RFC 2426";
    }

    @Override
    public @NlsSafe @NotNull String getDefaultExtension() {
        return "vcf";
    }

    @Override
    public Icon getIcon() {
        return VcardIcons.FILE;
    }
}
