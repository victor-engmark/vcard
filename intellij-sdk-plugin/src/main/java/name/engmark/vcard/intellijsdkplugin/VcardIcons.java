package name.engmark.vcard.intellijsdkplugin;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class VcardIcons {
    public static final Icon FILE = IconLoader.getIcon("/icons/logo.png", VcardIcons.class);
}

